package Jorge_Beneyto_Castelló;

import javax.swing.JOptionPane;
import Jorge_Beneyto_Castelló.Functions_exers;

public class Menu {

	public static void main(String[] args) {
		int option = 0, option1 = 0;
		String[] tipo = { "Divisors", "Digits", "Exit" };
		String[] tipo1 = { "Divisors", "Return ", "Exit" };
		String[] tipo2 = { "Digits", "Return ", "Exit" };
		String exer = "";
		JOptionPane.showMessageDialog(null, "Welcome to the exam of Jorge Beneyto Castelló ", "Welcome",JOptionPane.DEFAULT_OPTION);
		// Start the Exam
		do {
			option = functions.menu(tipo, " What option do you choose ?", "Primary Menu");
			// This is what happens when we press the x button
			if (option == -1){
				JOptionPane.showMessageDialog(null, "Exiting of the aplication ...", "Exiting",JOptionPane.INFORMATION_MESSAGE);
			System.exit(0);
			}else {
				//we entered to the primary menu
				switch (option) {
				case 0:
					do {
						// you call the exercise function
						exer = Functions_exers.exer_1("You are doing the exercise divisors", "Exercise Divisors");
						JOptionPane.showMessageDialog(null, exer, "Result", JOptionPane.INFORMATION_MESSAGE);
						// the secondary menu
						option1 = functions.menu(tipo1, "What do you want to do ?", "Secundary menu");
						// This is what happens when we exit of the secondary
						// menu
						if (option1 == -1 || option1 == 2) {
							JOptionPane.showMessageDialog(null, "Exiting of the aplication ...", "Exiting",JOptionPane.INFORMATION_MESSAGE);
							System.exit(0);
						}
					} while (option1 == 0);
					break;
				case 1:
					do {
						// you call the exercise function
						exer = Functions_exers.exer_2("You are doing the exercise digits", "Exercise Digits");
						JOptionPane.showMessageDialog(null, exer, "Result", JOptionPane.INFORMATION_MESSAGE);
						option1 = functions.menu(tipo2, "What do you want to do ?", "Secundary menu");
						// This is what happens when we exit of the secondary
						// menu
						if (option1 == -1 || option1 == 2) {
							JOptionPane.showMessageDialog(null, "Exiting of the aplication ...", "Exiting",JOptionPane.INFORMATION_MESSAGE);
							System.exit(0);
						}
					} while (option1 == 0);
					break;
				
				default:
					// This is what happens when we exit of the primary menu
					if (option == 2) {
						JOptionPane.showMessageDialog(null, "Closing application ...", "Exiting",JOptionPane.INFORMATION_MESSAGE);
						JOptionPane.showMessageDialog(null, "Thank You for using my java application.","See you soon!!", JOptionPane.INFORMATION_MESSAGE);// Condiciones
					}
				}// switch (option)
			} // end else

		} while ((option != 2) && (option1 != 2));
		//finish the exam
	}

}
