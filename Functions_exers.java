package Jorge_Beneyto_Castelló;

import javax.swing.JOptionPane;

import Jorge_Beneyto_Castelló.functions;

public class Functions_exers {

	public static String exer_1(String message, String title) {
		String cad = "", cad1 = "", cad2 = "";
		int num = 0, suma = 0, mayor = 0;
		boolean interruptor = false;

		while (interruptor == false) {
			num = functions.v_int("Number", "The number");
			if (num == -1)
				interruptor = true;
			else {
				suma = 0;
				for (int i = 1; i <= num; i++) {
					if ((num % i == 0) && ((i % 2 == 0))) {
						System.out.println("The ever divisors " + i);
						suma = suma + i;
						System.out.println("Las Suma" + suma);
					} // end if
				} // end for
				if (suma > mayor) {
					System.out.println(suma);
					mayor = suma;
				System.out.println("Major" + mayor);
				cad2 = ("The number is "+ num);
				}//end if
			} // end else
				cad1 = ("The most result of the addition of the divisors is " + mayor);
		} // end while
			cad = cad1+ cad2;
		return cad;
	}

	public static String exer_2(String message, String title) {
		String cad = "", cad1 = "", cadn = "";
		int num = 0, resto = 0, cont = 0, repite = 0, suma = 0, media = 0;

		do {
			num = functions.v_int("Can you put a number ?", "Number");
			cadn="";
			cadn= cadn+num;
			media = 0;
			suma = 0;
			cont = 0;
			while (num > 0) {
				resto = num % 10;
				num = num / 10;
				System.out.println(resto);
				if ((resto % 2) == 0) {
					suma = suma + resto;
					cont++;
					System.out.println("container" + cont);
				} // end if
			} // end while
			System.out.println("La suma" + suma);
			if (cont == 3) {
				System.out.println("cont" + cont);
				media = (suma / cont);
				System.out.println("La Media" + media);
				cad1 = cad1 + ("The result of the median of " + cadn + " is: " + media + "\n");
			} else {
				cad1 = cad1 + (cadn + " haven't three ever divisors"+ "\n");
			} // end else
			repite = JOptionPane.showConfirmDialog(null, " Do you want continue Y/N", "Continue", repite);
			if (repite == JOptionPane.NO_OPTION)
				break;
		} while (repite == JOptionPane.OK_OPTION);
		cad = cad1;
		return cad;
	}

}
