package Jorge_Beneyto_Castelló;

import javax.swing.JOptionPane;

public class functions {
	// -------------------------------------INT-------------------------------------
	// Function that ask for a number to the user and return an integer
	// -------------------------------------
	public static int v_int(String message, String title) {
		int num = 0;
		String s;
		boolean correcto = true;

		do {
			try {
				s = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				if (s == null) {
					JOptionPane.showMessageDialog(null, "You don't put a number", "Error", JOptionPane.ERROR_MESSAGE);
					correcto = false;
				} else {
					num = Integer.parseInt(s);
					correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "You don't put a number", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		System.out.println("You have put the number" + num);

		return num;
	}

	// -------------------------------------FLOAT-------------------------------------
	// Function that ask for a number to the user and return a float

	public static float v_float(String message, String title) {
		String s = "";
		float num = 0.0f;
		boolean correct = false;

		do {
			try {
				s = JOptionPane.showInputDialog(null, "Can you put the first number ?", "First Number",JOptionPane.QUESTION_MESSAGE);
				if (s == null) {
					JOptionPane.showMessageDialog(null, "You don't put a number", "Error", JOptionPane.ERROR_MESSAGE);
					correct = false;
				} else {
					num = Float.parseFloat(s);
					correct = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "You don't put a number", "Error", JOptionPane.ERROR_MESSAGE);
				correct = false;
			}
		} while (correct == false);
		System.out.println("You have put the  number" + num);

		return num;
	}

	// -------------------------------------CHAR---------------------------------------------
	/// Function that ask for a char to the user and return
	// it-------------------------------------
	public static char v_char(String message, String title) {
		char c = 0;
		String s;
		boolean correcto = true;

		do {
			try {
				s = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				if (s == null) {
					JOptionPane.showMessageDialog(null, "You don't put a letter", "Error", JOptionPane.ERROR_MESSAGE);
					correcto = false;
				} else {
					c = s.charAt(0);
					correcto = true;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "You don't put a letter", "Error", JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		System.out.println("You have put a valid chain" + c);

		return c;
	}

	// -------------------------------------STRING-------------------------------------
	/// Function that ask for a string and return it
	// -------------------------------------

	public static String v_String(String message, String title) {
		String s = "";
		boolean correcto = true;

		do {
			try {
				s = JOptionPane.showInputDialog(null, message, title, JOptionPane.QUESTION_MESSAGE);
				correcto = true;
				if (s == null) {
					JOptionPane.showMessageDialog(null, "Ypou don't put a String", "Error", JOptionPane.ERROR_MESSAGE);
					correcto = false;
				}
				if (s.equals("")) {
					JOptionPane.showMessageDialog(null, "Error de introducción de datos", "Error",
							JOptionPane.ERROR_MESSAGE);
					correcto = false;
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "No has introducido una cadena", "Error",JOptionPane.ERROR_MESSAGE);
				correcto = false;
			}
		} while (correcto == false);
		System.out.println("You have put a string" + s);

		return s;

	}

	// -------------------------------------MENU-------------------------------------
	// Menu buttons<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<//
	
	public static int menu(String[] tipo, String message, String title) {
		int operation = JOptionPane.showOptionDialog(null, message, title, 0, JOptionPane.QUESTION_MESSAGE, null, tipo,
				tipo[0]);
		
		return operation;
	}

}
